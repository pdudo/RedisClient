# RedisClient

#### 介绍
本仓库是用go编写的Redis客户端框架，是文章: https://juejin.cn/post/7140612873403203620/ 的实现。

#### 简单`Demo`

```go
package main

import (
	"fmt"
	"gitee.com/pdudo/RedisClient"
)

func main() {
	// 连接客户端
	client, err := RedisClient.Client("localhost:6379","123456",0)
	if err != nil {
		panic(err)
	}

	// Set
	// set name pdudo EX 60
	result , err := client.Set("name","pdudo","60","")
	if err != nil {
		fmt.Println("set 失败",err)
		return
	}
	fmt.Println("set 结果:",result)

	// Get
	// get name
	result , err = client.Get("name")
	if err != nil {
		fmt.Println("get 失败",err)
		return
	}
	fmt.Println("get 结果: " , result)

	// HSet
	// hset userInfo name pdudo
	// hset userInfo age 18
	client.HSet("userInfo","name","pdudo")
	client.HSet("userInfo","age","18")

	// HGet
	// hget userInfo name
	result , _ = client.HGet("userInfo","name")
	fmt.Println("HGet结果: " , result)

	// Run
	fmt.Println(client.Run("LPUSH","bob","v1","v2","v3"))
	fmt.Println(client.Run("LRANGE","bob","0","-1"))
}
```


执行结果
```bash
> go run main.go
连接Redis Server... , address: localhost:6379 , password: 123456 , port: 0
set操作 , key: name , val: pdudo , seconds: 60 , milliseconds:
set待发送的报文:  *5
$3
SET
$4
name
$5
pdudo
$2
EX
$2
60

set 结果: [OK]
get操作 , key: name
Get待发送报文:  *2
$3
GET
$4
name

get 结果:  [pdudo]
HGet结果:  [pdudo]
[6] <nil>
count: 6
[v3 v2 v1 v3 v2 v1] <nil>

>
```
